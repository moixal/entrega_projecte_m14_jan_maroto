# Entrega del projecte final d'Ionic.
## Jan Maroto.

[Enllaç del repositori.](https://codeberg.org/moixal/entrega_projecte_m14_jan_maroto)

[Enllaç del vídeo.](https://youtu.be/jrqG7MQcUYE)
[![](https://pad.nixnet.services/uploads/46b934c8-2fd5-4bcc-bb79-d0e8d2f342a7.png)](https://www.youtube.com/watch?v=jrqG7MQcUYE)


### Notes
* El *DELETE* i el *PUT* dels productes, està implementat per la part client, però falta implementar l'*endpoint* de la API.
* El projecte de Ionic, es troba a la carpeta 'BackOffice'.
* Els fitxers de l'API es troben a la carpeta 'Server'.
* La base de dades es troba en el fitxer 'database.sql'.
* El vídeo és el fitxer 'VideoTest.mp4' (També tens un enllaç en línia).
* Per a veure productes d'un usuari s'ha d'entrar amb l'usuari 'james' amb clau 'jamespass'.

### Coses implementades.
* Login.
* Logout.
* Registre.
* Llistar tots els productes.
* Llistar productes de l'usuari entrat.
* Mostrar el detall de cada producte.
* Editar el detall de cada producte (parcialment).
* Eliminar productes (parcialment).

### Coses per implementar.
* Entrar nous productes.
* Esborrar el compte (només per la part client).


