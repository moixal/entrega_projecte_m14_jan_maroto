-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 27, 2022 at 12:37 PM
-- Server version: 10.5.15-MariaDB-0+deb11u1
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `islanation`
--

-- --------------------------------------------------------

--
-- Table structure for table `islands`
--

CREATE TABLE `islands` (
  `id` int(10) NOT NULL,
  `name` varchar(10) NOT NULL,
  `description` text NOT NULL,
  `surface` int(10) NOT NULL,
  `latitude` bigint(30) NOT NULL,
  `longitude` bigint(30) NOT NULL,
  `country` varchar(255) NOT NULL,
  `population` int(20) NOT NULL,
  `images` varchar(1000) NOT NULL,
  `flag` varchar(50) NOT NULL,
  `price` int(20) NOT NULL,
  `owner` char(10) NOT NULL,
  `add_date` date NOT NULL,
  `visits` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `islands`
--

INSERT INTO `islands` (`id`, `name`, `description`, `surface`, `latitude`, `longitude`, `country`, `population`, `images`, `flag`, `price`, `owner`, `add_date`, `visits`) VALUES
(222222222, 'Greenland', 'Part of the Kingom of Denmark', 2342, 9403409, 30930934409, 'Denmark', 24234, '[\"picture-01.png\", \"picture-02.png\", \"picture-03.png\"]', 'flag', 123, '129232768', '2022-04-19', 21),
(823823482, 'Aruba', 'Part of the Kingdom of the Netherlands', 32000, 3121, 123, 'Netherlands', 17000000, '[\"picture-04.png\", \"picture-05.png\", \"picture-01.png\"]\n', 'flag.jpg', 900000000, '129232768', '2022-04-05', 22);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` char(10) NOT NULL,
  `nickname` varchar(32) NOT NULL,
  `pwd_hash` varchar(64) NOT NULL,
  `email` varchar(32) NOT NULL,
  `avatar` varchar(22) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `nickname`, `pwd_hash`, `email`, `avatar`) VALUES
('129232768', 'james', '$2y$10$VKNL8rTF9lWGGrMFYSt2vupyJwxa4YX.s8GVaZUpsKRbe/TEnHW82', 'james@james.james', 'default.jpg'),
('1827504477', 'jan', '$2y$10$VWlijvDRiLy4DCrJzdtgqe1TlQOML3knIS0JQsLXXYrdxwxoFoJaO', 'jan@jan.jan', 'default.jpg'),
('2345791988', 'xavi', '$2y$10$LurnnGoon/32HBAqCTtzCuYe6gh5MfHqirlsRVcvSeERHPzSMCyFS', 'jack@jack.jack', 'default.jpg'),
('42567045', 'marta', '$2y$10$bFdwEJsnUl.0qAC8Fwf6LeyoillYo42Gw1Dzb.IVSv9CNPxd0QCdG', 'marta@marta.marta', 'default.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `uuids`
--

CREATE TABLE `uuids` (
  `uuid` char(36) NOT NULL,
  `user` char(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `uuids`
--

INSERT INTO `uuids` (`uuid`, `user`) VALUES
('f9d41409-9eb2-4422-8203-db684ec80918', '129232768'),
('437b1184-d23b-4744-adda-756ec7373866', '129232768'),
('600a3463-ae63-4c05-be1c-56d8786315ae', '129232768'),
('ba4cc990-b5f7-4dad-b349-6a228d9d8351', '129232768'),
('7f96957e-9ab8-417d-bf37-6f1772f01ad9', '129232768'),
('18bd0180-3b4d-4588-bda7-ee7ebf8f2f4d', '129232768'),
('1d679d36-8dd0-4331-95fe-b7d3113b136f', '129232768'),
('88eecf3f-0144-4f5a-9277-11ccdb912ff6', '129232768'),
('22d3ba7e-7571-45ae-9b0d-b1dc46f4bc0c', '129232768'),
('e3491be1-282a-4332-9a38-88802e954997', '129232768'),
('f469db12-aaba-491a-9956-8bc1a5e8debf', '129232768'),
('10d127b7-4c0b-4668-aceb-d68d29398d47', '129232768'),
('219a2767-4022-4189-b982-0b8f9669f998', '129232768'),
('f580e402-ba3f-46e6-8044-6b31887ee2db', '1827504477'),
('e6f14339-3601-45f7-bea3-dde7a4219839', '1827504477'),
('1db6ef84-bb6d-4faa-82f1-6dfc9842e639', '129232768'),
('4b5a69d5-0b5d-4064-ba18-6317ebdf9511', '129232768'),
('b9d33c3f-037c-4494-af8b-d8a9d6176a5f', '129232768'),
('8a3e95fe-27fa-4ccf-81aa-1982f74a9f9c', '129232768'),
('c00a426e-9c9a-4bbf-b7d4-b3b7b62ad60d', '129232768'),
('465279fc-607b-4cd4-a29e-cd99f6d40627', '129232768'),
('fb569f58-8960-404d-9adc-ca3c57f6bad7', '42567045');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `islands`
--
ALTER TABLE `islands`
  ADD PRIMARY KEY (`id`),
  ADD KEY `islands_ibfk_1` (`owner`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `uuids`
--
ALTER TABLE `uuids`
  ADD KEY `user` (`user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `islands`
--
ALTER TABLE `islands`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=823823483;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `islands`
--
ALTER TABLE `islands`
  ADD CONSTRAINT `islands_ibfk_1` FOREIGN KEY (`owner`) REFERENCES `users` (`id`);

--
-- Constraints for table `uuids`
--
ALTER TABLE `uuids`
  ADD CONSTRAINT `uuids_ibfk_1` FOREIGN KEY (`user`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
